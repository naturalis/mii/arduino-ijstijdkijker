### Instructies uploaden firmware
Deze instructies zijn voor uploaden van de firmware via Arduino IDE. Als je Platform IO (IDE) wilt gebruiken, zul je zelf moeten uitzoeken hoe deze instructies daarnaar vertaald moeten worden.

- In de Arduino IDE Preferences, voeg de volgende regel toe aan de 'Additional Board Manager URLs':
https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json
- In de Boards Manager zoek naar 'sparkfun' en voeg de 'Sparkfun AVR Boards' toe.
- Kies als board de 'SparkFun Pro Micro'.
- Kies als processor de 'ATmega32U4 (5V, 16 MHz)'.
- Kies de bijbehorende COM (of /dev/tty) poort.
- Zorg dat de bijgevoegde library 'AS5048A-Arduino' gevonden kan worden door de IDE.
- Compileer en upload.

### Alternatief
- Gebruik de bijgevoegde .hex code om te uploaden naar het Arduino board. 