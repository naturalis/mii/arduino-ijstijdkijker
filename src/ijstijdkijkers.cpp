//#define ONE_SENSOR 1
#include <AS5048A.h>

const int PIN_SWITCH = 4;
const int PIN_LED1 = 5;
const int PIN_LED2 = 6;
const int PIN_LED3 = 9;
const int PIN_SCK_SEN1 = 10;
const int PIN_SCK_SEN2 = 18;
const int PIN_YELLOW_LED = 17;

// the sensor CSn pin is connected to the PIN_SCK_SEN*
AS5048A angleSensor1(PIN_SCK_SEN1);
AS5048A angleSensor2(PIN_SCK_SEN2);

int button = 0;
word val1 = 0;
word val2 = 0;
word oldval1 = 0;
word oldval2 = 0;
int timer = 0;
byte ledvalue = 0;
bool ledOn = false;

void setup()
{
  Serial.begin(115200);
  angleSensor1.init();
  angleSensor1.printState();
  angleSensor1.getErrors();
#if !defined(ONE_SENSOR)
  angleSensor2.init();
  angleSensor2.printState();
  angleSensor2.getErrors();
#endif
  pinMode(PIN_SWITCH, INPUT);
  pinMode(PIN_LED1, OUTPUT);
  pinMode(PIN_LED2, OUTPUT);
  pinMode(PIN_LED3, OUTPUT);
  pinMode(PIN_YELLOW_LED, OUTPUT);
}

void loop()
{
  button = digitalRead(PIN_SWITCH);
  val1 = angleSensor1.getRawRotation();
#if !defined(ONE_SENSOR)
  val2 = angleSensor2.getRawRotation();
#endif

  Serial.print(val1);
  Serial.print(" ");
  Serial.print(val2);
  Serial.print(" ");
  Serial.println(button);

  if (ledvalue < 255 && ledOn)
  {
    ledvalue++;
  }
  else if (ledvalue > 0 && !ledOn)
  {
    ledvalue--;
  }
  analogWrite(PIN_LED1, ledvalue);
  analogWrite(PIN_LED2, ledvalue);

  if (abs(val1 - oldval1) > 100)
  {
    ledOn = true;
    oldval1 = val1;
    timer = 1000; // 10 seconds
  }
  if (abs(val2 - oldval2) > 100)
  {
    ledOn = true;
    oldval2 = val2;
    timer = 1000; // 10 seconds
  }

  if (timer > 0)
  {
    timer--;
  }
  else
  {
    ledOn = false;
  }
  
  delay(10);
}
